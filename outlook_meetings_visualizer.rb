require 'sqlite3'
require 'win32ole'
require 'json'
require 'launchy'
require 'fileutils'

class OutlookMeetingsVisualizer

  DB_DATE     = '%Y-%m-%d'
  DB_DATETIME = DB_DATE + ' %H:%M:%S.%L'
  INSERT_SQL  = 'INSERT INTO appointments (subject, location, start_datetime, duration, end_datetime, body) VALUES (?, ?, ?, ?, ?, ?)'

  def open_database
    db_path = File.join(File.dirname(__FILE__), 'db/calendar.sqlite3')
    FileUtils.rm(db_path) if File.exist?(db_path)
    @db = SQLite3::Database.new(db_path)
  end

  def close_database
    @db.close
  end

  def create_calendar_table
    @db.execute <<-SQL
CREATE TABLE appointments (
    subject VARCHAR(255),
    location VARCHAR(255),
    start_datetime VARCHAR(23),
    duration INT,
    end_datetime VARCHAR(23),
    body BLOB
);
    SQL
  end

  def insert_appointments
    # http://rubyonwindows.blogspot.com/2007/07/automating-outlook-with-ruby-calendar.html
    outlook  = WIN32OLE.new('Outlook.Application')
    mapi     = outlook.GetNameSpace('MAPI')
    calendar = mapi.GetDefaultFolder(9)

    calendar.Items.each do |appointment|
      subject        = appointment.Subject
      location       = appointment.Location
      start_datetime = appointment.Start.strftime(DB_DATETIME)
      duration       = appointment.Duration
      end_datetime   = appointment.End.strftime(DB_DATETIME)
      body           = appointment.Body
      @db.execute(INSERT_SQL, [subject, location, start_datetime, duration, end_datetime, body])
    end
  end

  def select_data
    # http://www.sqlite.org/lang_datefunc.html
    sql = <<-SQL
SELECT
    start_date,
    SUM(duration)
FROM
    (
        SELECT
            duration,
            strftime('#{DB_DATE}', start_datetime) start_date
        FROM
            appointments
        WHERE
            duration < 1440                                                               -- Only if less than 1 day
        AND subject <> 'Lunch'                                                            -- Ignore lunch block
        AND strftime('%w', start_datetime) in ('1', '2', '3' ,'4', '5')                   -- Only workdays (Mon-Fri)
        AND strftime('%w', end_datetime) in ('1', '2', '3' ,'4', '5')                     -- Only workdays (Mon-Fri)
        AND strftime('#{DB_DATE}', start_datetime) = strftime('#{DB_DATE}', end_datetime) -- start/end on same day
    )
GROUP BY
    start_date
    SQL
    @data = @db.execute(sql)
  end

  def save_index_html
    File.open('output/index.html', 'wt') {|f| f.write(index_html)}
  end

  def open_index_html
    Launchy.open 'output/index.html'
  end

  def run
    open_database
    create_calendar_table
    insert_appointments
    select_data
    close_database
    save_index_html
    open_index_html
  end

  def data_for_nvd3
    meetings = { key: 'Meetings', values: @data.map {|row| { x: row[0], y: row[1] }} }
    workday  = { key: 'Workday', values: @data.map {|row| { x: row[0], y: 480 - row[1] }} }
    "#{meetings.to_json},\n#{workday.to_json}"
  end

  def nvd3_chart_javascript
    <<-JAVASCRIPT
    var data = [
#{data_for_nvd3}
    ];

nv.addGraph(function() {
    var chart = nv.models.multiBarChart()
      .transitionDuration(350)
      .reduceXTicks(true)   // If 'false', every single x-axis tick label will be rendered.
      .rotateLabels(0)      // Angle to rotate x-axis labels.
      .showControls(false)  // Allow user to switch between 'Grouped' and 'Stacked' mode.
      .groupSpacing(0.1)    // Distance between each group of bars.
      .stacked(true);

    chart.yAxis
        .tickFormat(d3.format(',d'));

    d3.select('#chart svg')
        .datum(data)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});
    JAVASCRIPT
  end

  def index_html
    <<-HTML
<!DOCTYPE html>
<html>
<head>
    <title>Meetings Visualization</title>
    <link href="stylesheets/nv.d3.min.css" media="screen, projection" rel="stylesheet" type="text/css">
    <script src="javascripts/jquery-1.11.1.min.js"></script>
    <script src="javascripts/d3.min.js"></script>
    <script src="javascripts/nv.d3.min.js"></script>
</head>
<body>
<div id='chart'>
  <svg style='height:500px'></svg>
</div>
<script type="text/javascript">
#{nvd3_chart_javascript}
</script>
</body>
</html>
    HTML
  end

end

if __FILE__ == $PROGRAM_NAME
  OutlookMeetingsVisualizer.new.run
end
