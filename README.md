# Outlook Meetings Visualizer

A ruby script which visualizes the amount of time meetings take up for each workday. Calendar data is extracted from your local Outlook running on Windows.

Refer to my blog post for more information:  [Visualize your Outlook meetings using Ruby and NVD3](https://ejstembler.com/2014/07/visualize-outlook-meetings-using-ruby-nvd3/)

## How it works

The script extracts Outlook calendar items using Microsoft [OLE Automation](http://en.wikipedia.org/wiki/OLE_Automation) via the [win32ole](http://ruby-doc.org/stdlib-1.9.3/libdoc/win32ole/rdoc/WIN32OLE.html) module.  The data is saved in a temporary [SQLite3](http://www.sqlite.org) database at <tt>db/calendar.sqlite3</tt>.

Appointments are selected, and summed, from the database with the following criteria:

* Only if less than 1 day
* Ignore lunch block
* Only workdays (Mon-Fri)
* start/end on same day

The data is normalized, and converted to JSON for use in a [NVD3](http://nvd3.org) [MultiBarChart](http://nvd3.org/examples/multiBar.html).  An index.html file is generated in <tt>output/index.html</tt> which renders the NVD3 MultiBarChart.

![screenshot](screenshot.png?raw=true)

## Requirements

This ruby script only runs on Windows where Outlook is installed.  It has been tested using ruby 1.9.3 on Windows 7 x64.

* [Ruby 1.9.3-p545](http://dl.bintray.com/oneclick/rubyinstaller/rubyinstaller-1.9.3-p545.exe?direct)
* [git](http://git-scm.com/download/win) or [GitHub Client for Windows](https://windows.github.com)

## Retrieving source

From the Windows command-line, clone the project from GitHub:

```shell
git clone https://gitlab.com/ejstembler/outlook-meetings-visualizer.git
```

## Installing dependencies

Install [bundler](http://bundler.io) 1.6.3 if you don't already have it installed:

```shell
gem install bundler
```

Run bundler before attempting to run the script:

```shell
bundle install
```

## Running the script

```shell
ruby outlook_meetings_visualizer.rb
```